module.exports = function (grunt) {
    'use strict';

    // Load grunt tasks automatically
    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        autoprefixer: {
            app: {
                files: {
                    './temp/assets/css/styles.css': './temp/assets/css/styles.css'
                }
            }
        },
		
		browserify: {
			'./components.js': ['./index.js']
		},

        clean: {
            afterbuild: './temp',
            browserify: './components.js',
            prebuild: ['./dist/*', './temp/*']
        },

        copy: {
            app: {
				files: [
					{expand: true, src: './components.js', dest: './temp/assets/js/'}
				]
            },
            prod: {
                files: [
                    {expand: true, cwd: './temp/', src: '**/*', dest: './dist/'},
                    {expand: true, cwd: './src/', src: ['404.html', 'robots.txt'], dest: './dist/'},
                    {expand: true, cwd: './src/assets/', src: '**', dest: './dist/assets'},
                    {expand: true, cwd: './node_modules/font-awesome/fonts/', src: '*.*', dest: './dist/assets/fonts'},
                    {expand: true, cwd: './node_modules/bootstrap-sass/assets/fonts/bootstrap', src: '*.*', dest: './dist/assets/fonts'}
                ]
            }
        },

        express: {
            app: {
                options: {
                    port: 9000,
                    hostname: '0.0.0.0',
                    bases: ['temp', 'src'],
                    livereload: true,
                    open: true
                },
            },
            prod: {
                options: {
                    port: 9001,
                    bases: 'dist',
                    livereload: false,
                    open: true
                }
            }
        },

        filerev: {
            scripts: {
                src:[
                    './temp/**/*.js'
                ]
            },
            styles: {
                src: [
                    './temp/**/*.css'
                ]
            }
        },

        injector: {
            options: {
                destFile: 'temp/index.html',
                ignorePath: 'src/'
            },
            app: {
                files: {
                    src: ['src/app/**/*.js']
                }
            }
        },

        jshint: {
            options: {
                reporter: require('jshint-stylish')
            },
            all: [
                './src/app/**/*.js'
            ]
        },

        processhtml: {
            options: {},
            app: {
                files: {
                    './temp/index.html': ['./src/index.html']
                }
            }
        },

        sass: {
            options: {
                noCache: true,
                sourcemap: 'none',
                style: 'compressed'
            },
            app: {
                files: {
                    './temp/assets/css/styles.css': './src/app/base.scss'
                }
            }
        },

        scsslint: {
            allFiles: [
                './src/**/*.scss'
            ],
            options: {
                bundleExec: false
            }
        },
		
		tinyimg: {
            app: {
                files: [{
                    expand: true,
                    cwd: './dist/assets/images/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: './dist/assets/images/'
                }]
            }
        },

        usemin: {
            html: ['./temp/index.html']
        },

        useminPrepare: {
            options: {
                dest: './temp',
                root: './src'
            },
            html: './temp/index.html'
        },

        watch: {
            options: {
                livereload: true,
                interval: 5007
            },
            css: {
                files: [
                    './temp/assets/**/*.css'
                ]
            },
            html: {
                files: [
                    './src/**/*.html'
                ],
                tasks: [
                    'browserify',
                    'processhtml',
                    'copy:app',
                    'injector'
                ]
            },
            js: {
                files: [
                    './src/app/**/*.js',
                    './src/app/*.js'
                ]
            },
            sass: {
                files: [
                    './src/**/*.scss'
                ],
                tasks: [
                    'sass:app',
                    'autoprefixer:app'
                ],
                options: {
                    livereload: false
                }
            }
        }
    });

    grunt.registerTask('default', []);

    grunt.registerTask('build', [
        'clean:prebuild',
		'browserify',
        'processhtml',
        'copy:app',
        'sass:app',
        'autoprefixer:app',
        'injector',
        'useminPrepare',
        'concat:generated',
        'uglify:generated',
        'filerev',
        'usemin',
        'copy:prod',
        'tinyimg:app',
        'clean:afterbuild',
        'clean:browserify'
    ]);

    grunt.registerTask('localbuild', [
        'clean:prebuild',
		'browserify',
        'processhtml',
        'copy:app',
        'sass:app',
        'autoprefixer:app',
        'injector',
        'clean:browserify'
    ]);

    grunt.registerTask('server', ['localbuild', 'express:app', 'watch']);
    grunt.registerTask('server:dev', ['server']);
    grunt.registerTask('server:prod', ['build', 'express:prod', 'express-keepalive']);

    grunt.registerTask('test', ['jshint', 'scsslint']);

    grunt.registerTask('sasslint', ['scsslint']);
};
